#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

if __name__ == "__main__":
    if not os.getenv("DJANGO_SETTINGS_MODULE"):
        raise RuntimeError("You need to set the DJANGO_SETTINGS_MODULE environment variable")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
