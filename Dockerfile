FROM python:3.8-slim-buster
RUN apt-get update -y
RUN apt-get install -y gnupg curl gcc libpq-dev zlib1g-dev libssl-dev
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install -y nodejs
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt update
RUN apt install yarn
RUN pip install -U pip
COPY . /app
WORKDIR /app
RUN pip install -r dev-requirements.txt
RUN yarn install
