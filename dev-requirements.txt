-r requirements.txt
django-extensions==3.0.9
flake8==3.8.3
ipython==7.18.1
mypy==0.782
pdbpp==0.10.2
pylint==2.6.0
pylint-django==2.3.0
pytest==6.1.0
pytest-cov==2.10.1
pytest-django==3.10.0
tox==3.20.0
