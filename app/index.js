
import React from 'react';
import ReactDOM from 'react-dom';

import IconLabelButtons from './button'
import TrackInTable from './table'

import './index.css';
  
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          stores: []
        };
    }
    componentDidMount() {
        fetch('http://127.0.0.1:8000/stores')
            .then(response => response.json())
            .then(data => this.setState({ stores: data.results }));
    }
    render() {
        return(
            <div>
                <IconLabelButtons />
                <TrackInTable stores={this.state.stores} />
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))
