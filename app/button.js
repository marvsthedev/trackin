import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles((theme) => ({
    button: {
      margin: theme.spacing(1),
      marginBottom: theme.spacing(5),
      borderRadius: 25,
      border: 0,
      color: 'white',
      height: 48,
      padding: '0 30px',
    },
}));
  
export default function IconLabelButtons() {
    const classes = useStyles();
  
    return (
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          startIcon={<AddIcon />}
        >
          Add Stores
        </Button>
    )
}