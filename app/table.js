import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TrendingUpOutlinedIcon from '@material-ui/icons/TrendingUpOutlined';
import { fontSize } from '@material-ui/system';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
    svg: {
        color: 'lightgray',
        border: '1px solid lightgray',
        borderRadius: '5px',
        fontSize: 'medium'
    },
});


export default function TrackInTable(props) {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                    <TableCell className='bold'>Your stores</TableCell>
                    <TableCell align="right">Total footfall <TrendingUpOutlinedIcon className={classes.svg}/></TableCell>
                    <TableCell align="right">New footfall <TrendingUpOutlinedIcon className={classes.svg}/></TableCell>
                    <TableCell align="right">Returning footfall <TrendingUpOutlinedIcon className={classes.svg}/></TableCell>
                    <TableCell align="right">Total time spent in the shop <TrendingUpOutlinedIcon className={classes.svg}/></TableCell>
                    <TableCell align="right">Total departments visited <TrendingUpOutlinedIcon className={classes.svg}/></TableCell>
                    <TableCell align="right">Average department visit time <TrendingUpOutlinedIcon className={classes.svg}/></TableCell>
                    <TableCell align="right">Bounce rates <TrendingUpOutlinedIcon className={classes.svg}/></TableCell>
                    <TableCell className='bold' align="right">Store Performance</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.stores.map((store) => (
                        <TableRow key={store.store_name} hover="true" className="test">
                            <TableCell scope="row"><Paper elevation={3}>{store.store_name}</Paper></TableCell>
                            <TableCell align="right">{store.total_footfall}</TableCell>
                            <TableCell align="right">{store.new_footfall}</TableCell>
                            <TableCell align="right">{store.returning_footfall}</TableCell>
                            <TableCell align="right">{store.total_time_spent} h</TableCell>
                            <TableCell align="right">{store.total_departments_visited} / {store.total_number_of_department}</TableCell>
                            <TableCell align="right">{store.average_department_visit_time} h</TableCell>
                            <TableCell align="right">{store.bounce_rate} %</TableCell>
                            <TableCell align="right">{store.ranking}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}