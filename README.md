# To bring up Docker
`docker-compose up` or `docker-compose up --build` if first time running it

# Load data
`docker-compose exec web bash`
`python manage.py loaddata stores`

Frontend should now be reachable at http://localhost:4000/ and backend at http://localhost:8000
