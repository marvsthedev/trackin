from django.contrib import admin

from .forms import StoresForm
from .models import Stores


class StoresAdmin(admin.ModelAdmin):
    model = Stores
    form = StoresForm


admin.site.register(Stores, StoresAdmin)
