from rest_framework import serializers

from .models import Stores


class StoresSerializer(serializers.ModelSerializer):
    ranking = serializers.ReadOnlyField()

    class Meta:
        model = Stores
        fields = (
            'id', 'store_name', 'total_footfall', 'new_footfall',
            'returning_footfall', 'total_time_spent', 'total_departments_visited',
            'total_number_of_department', 'average_department_visit_time',
            'bounce_rate', 'ranking'
        )

