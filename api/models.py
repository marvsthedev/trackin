from django.db import models
from django.db.models import Count


class Stores(models.Model):
    store_name = models.CharField(max_length=50)
    total_footfall = models.IntegerField()
    new_footfall = models.IntegerField()
    returning_footfall = models.IntegerField()
    total_time_spent = models.FloatField()  
    total_departments_visited = models.IntegerField()
    total_number_of_department = models.IntegerField()
    average_department_visit_time = models.FloatField()
    bounce_rate = models.FloatField()

    @property
    def ranking(self):
        qs = self.__class__.objects.filter(total_time_spent__gt=self.total_time_spent)
        aggregate = qs.aggregate(ranking=Count('total_time_spent'))
        return aggregate['ranking'] + 1
