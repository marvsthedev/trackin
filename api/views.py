from rest_framework import viewsets

from .models import Stores
from .serializers import StoresSerializer


class StoresViewSet(viewsets.ModelViewSet):
    """
    View Set for Stores.
    """
    http_method_names = ['get']
    serializer_class = StoresSerializer

    def get_queryset(self):
        """ Limit Stores to those in the users groups. """
        return Stores.objects.all()
