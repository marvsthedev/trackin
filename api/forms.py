from django import forms

from .models import Stores


class StoresForm(forms.ModelForm):
    class Meta:
        model = Stores
        fields = '__all__'
