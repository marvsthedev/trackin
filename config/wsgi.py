"""
WSGI config for trackin project.
"""

import os

from django.core.wsgi import get_wsgi_application

if __name__ == '__main__':
    settings = os.environ.get("DJANGO_SETTINGS_MODULE")
    if not settings:
        raise RuntimeError(
            "You need to set the DJANGO_SETTINGS_MODULE environment variable")


application = get_wsgi_application()
