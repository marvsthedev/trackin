from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*']

HOSTNAME = 'https://127.0.0.1:8000'

URL_STARTS = 'http://127.0.0.1:8000'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'trackin',
        'USER': 'trackin',
        'PASSWORD': 'trackin',
        'HOST': 'localdb',
        'PORT': '5432',
    }
}

INTERNAL_IPS = ('127.0.0.1',)
