"""trackin URL Configuration
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from api.views import StoresViewSet

stores_router = routers.DefaultRouter()
stores_router.register(r'stores', StoresViewSet, basename='stores')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(stores_router.urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
